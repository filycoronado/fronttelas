import { GridExampleComponent } from './grid-example/grid-example.component';
import { Routes } from '@angular/router';

export const TestRoutes: Routes = [
  { path: 'gridTest', component: GridExampleComponent }
];
