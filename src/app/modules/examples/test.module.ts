import { GridExampleComponent } from './grid-example/grid-example.component';
import { AppGridComponent } from './../../Core/utilities/app-grid/app-grid.component';
import { UtilitiesModule } from './../../Core/utilities.module';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { TestRoutes } from './test.routing';
@NgModule({
  imports: [
    RouterModule.forChild(TestRoutes),
    UtilitiesModule
  ],
  declarations: [GridExampleComponent],
  exports: []
})
export class TestModule { }
