import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-grid-example',
  templateUrl: './grid-example.component.html',
  styleUrls: ['./grid-example.component.scss']
})
export class GridExampleComponent implements OnInit {

  data: any[]  = [
    {id: 1, color: 'prieto', brand: 'nissan', model: '2010', name: 'tsuru'},
    {id: 1, color: 'coral', brand: 'kia', model: '2010', name: 'spark'},
    {id: 1, color: 'verde mayate', brand: 'chevrolet', model: '2010', name: 'camaro'},
    {id: 1, color: 'azul puto', brand: 'ford', model: '2010', name: 'malibú'},
  ];

  columns: any[] = [
    { header: 'ID', field: 'id', visible: false },
    { header: 'Color', field: 'color', visible: true },
    { header: 'Marca', field: 'brand', visible: true },
    { header: 'Modelo', field: 'model', visible: true },
    { header: 'Tipo', field: 'name', visible: true }
   ];
  constructor() { }

  ngOnInit() {
  }

}
