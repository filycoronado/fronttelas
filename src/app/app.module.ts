import { UtilitiesModule } from './Core/utilities.module';
import { ReportsService } from './services/reports.service';
import { NotificationsService } from './services/notifications.service';
import { DashboardModule } from './pages/dashboard/dashboard.module';
import { UserService } from './services/user.service';
import { TestModule } from './modules/examples/test.module';
import { ErrorInterceptor } from './services/error-interceptor.service';
import { JwtInterceptor } from './services/jwt-interceptor.service';
import { LoginComponent } from './layouts/admin-layout/login/login.component';
import { QuickAppProMaterialModule } from './modules/material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ToastrModule } from 'ngx-toastr';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { SidebarModule } from './sidebar/sidebar.module';
import { FooterModule } from './shared/footer/footer.module';
import { NavbarModule} from './shared/navbar/navbar.module';
import { FixedPluginModule} from './shared/fixedplugin/fixedplugin.module';


import { AppComponent } from './app.component';
import { AppRoutes } from './app.routing';

import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { GenericApiCallService } from './Core/global/generic-api-call.service';
import { GlobalApiService } from './Core/global/global-service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GlobalServiceModule } from './Core/global/globa-service.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { EmployeeLayoutModule } from './layouts/employee-layout/employee-layout.module';
import { MachineLayoutModule } from './layouts/machine-layout/machine-layout.module';
import { ReportLayoutModule } from './layouts/report-layout/report-layout.module';



@NgModule({
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(AppRoutes, {
      useHash: true
    }),
    SidebarModule,
    NavbarModule,
    ToastrModule.forRoot(),
    FooterModule,
    FixedPluginModule,
    FormsModule,
    ReactiveFormsModule,
    GlobalServiceModule.forRoot(),
    QuickAppProMaterialModule,
    TestModule,
    FlexLayoutModule,
    EmployeeLayoutModule,
    DashboardModule,
    MachineLayoutModule,
    ReportLayoutModule,
    UtilitiesModule
  ],
  providers: [GenericApiCallService, GlobalApiService, UserService, NotificationsService, ReportsService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }
