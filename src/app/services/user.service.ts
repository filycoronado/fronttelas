import { User } from './../model/user';
import { Injectable, OnInit } from '@angular/core';

@Injectable()
export class UserService implements OnInit {
  loggedUser: any;
  permissions: any[];
  constructor() {
  }

  ngOnInit() {
    this.user = JSON.parse(sessionStorage.getItem('currentUser'));
    this.permissions = this.loggedUser.permissions;
  }

  getUser(): string {
    this.loggedUser = JSON.parse(sessionStorage.getItem('currentUser'));
    return this.loggedUser.data.name;
  }

  getRole(): string {
    return this.loggedUser.data.role;
  }

  hasPermission(level: string, permission: string): boolean {
    if (this.permissions !== undefined && this.permissions.length > 0) {
      const p = this.permissions[level];
      if (p !== undefined && p.length > 0) {
        if (p[permission] !== undefined && p[permission] !== '') {
          return true;
        }
      }
    }
    return false;
  }

  set user(u: any) {
    this.loggedUser = u;
  }
}
