import { NavbarModule } from './../../shared/navbar/navbar.module';
import { FixedPluginModule } from './../../shared/fixedplugin/fixedplugin.module';
import { FooterModule } from './../../shared/footer/footer.module';
import { FormsModule } from '@angular/forms';
import { SidebarModule } from './../../sidebar/sidebar.module';
import { EmployeeLayoutRoutes } from './../../layouts/employee-layout/employee-layout.routing';
import { UtilitiesModule } from './../../Core/utilities.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
    imports: [
      RouterModule,
      CommonModule,
      UtilitiesModule,
      SidebarModule,
      FooterModule,
      FormsModule,
      FixedPluginModule,
      NavbarModule,
      NgbModule],
    declarations: [ DashboardComponent ],
    exports: [ DashboardComponent ]
})

export class DashboardModule {}
