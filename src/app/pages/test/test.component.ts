import { GlobalApiService } from './../../Core/global/global-service';
import { User } from './../../model/user';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})
export class TestComponent implements OnInit {
  // Form variables
  testGroup: FormGroup;

  // Variables
  user: User;
  isEdit = false;
  id: number;

  constructor(private formBuilder: FormBuilder,
              private globalService: GlobalApiService,
              private route: ActivatedRoute) {

                this.initForm();
               }

  ngOnInit() {
    this.route.params.subscribe(params => {
      const id = params['id'];
      if (id && id > 0) {
        this.id = id;
        this.globalService.routes.users.getById()<any>(id).subscribe(
          response => {
             this.user = response;
             this.patchValues(this.user);
            }
        );
      }
    });
  }

  initForm(): void {
    this.testGroup = this.formBuilder.group({
      name: ['', [Validators.required]],
      lastName: [''],
      address: [''],
      email: [''],
      phone: [''],
      controlNumber: ['']
    });
  }

  patchValues(p: User): void {
    this.testGroup.patchValue({
      name: p.username,
      lastName: p.password,
    });
  }

  saveOrUpdate(): void {
    if (this.testGroup.valid) {
    const p = this.parseData();
      this.isEdit ? this.update(p) : this.save(p);
    }
  }

  save(p: User): void {
    this.globalService.routes.person.addPerson()<User>(p).subscribe(
      response => { },
      error => { }
    );
  }

  update(p: User): void {

  }

  private parseData(): User {
    const p: User = {
      id: this.id,
      username: this.testGroup.get('name').value,
      password: this.testGroup.get('lastName').value,
    }
    return p;
  }

  getEmployeeById(): void {
    this.globalService.routes.employee.getById()<User>(1).subscribe(
      response => { }
    );
  }

  getByName(): void {
   this.globalService.routes.employee.getByName('fily')().subscribe(
    (response: User) => { }
   );
  }

  getAll(): void {
    this.globalService.routes.employee.getAll()().subscribe(
      response => { },
      error => { }
    );
  }

  addEmployee(): void {
    this.globalService.routes.employee.addEmployee()(null).subscribe();
  }

}
