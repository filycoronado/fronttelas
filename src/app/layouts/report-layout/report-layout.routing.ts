import { Routes } from '@angular/router';
import { ReportLayoutComponent } from './report-layout.component';


export const ReportLayoutRoutes: Routes = [
    { path: 'view', component: ReportLayoutComponent }
];
