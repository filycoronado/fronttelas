import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UtilitiesModule } from './../../Core/utilities.module';
import { QuickAppProMaterialModule } from './../../modules/material.module';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReportLayoutRoutes } from './report-layout.routing';
import { ReportLayoutComponent } from './report-layout.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ReportLayoutRoutes),
    FormsModule,
    NgbModule,
    QuickAppProMaterialModule,
    ReactiveFormsModule,
    UtilitiesModule
  ],
  declarations: [
      ReportLayoutComponent
  ]
})

export class ReportLayoutModule {}
