import { Component, OnInit } from '@angular/core';
import { GlobalApiService } from '../../Core/global/global-service';
import Chart from 'chart.js';
import { ReportsService } from '../../services/reports.service';
import { environment as config } from '../../../environments/environment';

@Component({
  selector: 'app-report-layout',
  templateUrl: './report-layout.component.html',
  styleUrls: ['./report-layout.component.scss']
})
export class ReportLayoutComponent implements OnInit {
  data: any[] = [];
  chartMetersOperator: any = {};
  chartMetersMachine: any = {};
  chartUrdimbreOperator: any = {};
  chartUrdimbreMachine: any = {};
  reportUrdim: any = {};
  reportEfi: any = {};
  public canvas: any;
  public ctx;
  public chartColor;
  public chartEmail;
  public chartHours;
  constructor(
    private globalService: GlobalApiService,
    private reportService: ReportsService
    ) { }

  ngOnInit() {
    this.globalService.routes.reports.getChartsData()<any[]>().subscribe(
      response => {

        this.data = response['data'];
        console.log(this.data);
        this.getReportMetersOperators();
        this.getReportMetersMachines();
        this.getReportUrdimbreOperators();
        this.getReportUrdimbreMachines();
        /* this.getReportMeters();
        this.getReportUrdim();
        this.getReportEfi(); */
      },
      error => { }
    );
  }

  // Grafica de barras para metros por operador
  getReportMetersOperators(): void {
    console.log(this.data['data'] );
    let arraydata: any[] = [];
    let arraylabels: any[] = [];
    let arraybg: any[] = [];
    let ids: any[] = [];
    for (let i = 0; i < this.data.length; i++) {
      const element = this.data[i];
      //console.log(ids.includes(element.operator_id));
      if (!ids.includes(element.operator_id)) {
        ids[i] = element.operator_id;
        console.log(element.operator_id);
        arraydata[i] = element.meters;
        arraylabels[i] = element.operator;
        arraybg[i] = this.reportService.getHexRandomColor();
      } else {
        let index = ids.indexOf(element.operator_id);
        console.log(index);
        console.log(arraydata[index]);
        arraydata[index] = arraydata[index] + element.meters;
      }
      //console.log(ids);
      
    }
    this.chartMetersOperator.labels = arraylabels;
    this.chartMetersOperator.data = arraydata;
    this.chartMetersOperator.backgrounds = arraybg;
    this.chartMetersOperator.title = 'Produccion';
    console.log(this.chartMetersOperator);
    let canvas = document.getElementById("chartMetersOperator");
    this.reportService.barChart(canvas, this.chartMetersOperator);
  }
  
  // Grafica de barras para metros por maquina
  getReportMetersMachines(): void {
    console.log(this.data['data'] );
    let arraydata: any[] = [];
    let arraylabels: any[] = [];
    let arraybg: any[] = [];
    let ids: any[] = [];
    for (let i = 0; i < this.data.length; i++) {
      const element = this.data[i];
      //console.log(ids.includes(element.machine_id));
      if (!ids.includes(element.machine_id)) {
        ids[i] = element.machine_id;
        console.log(element.machine_id);
        arraydata[i] = element.meters;
        arraylabels[i] = element.machine_name;
        arraybg[i] = this.reportService.getHexRandomColor();
      } else {
        let index = ids.indexOf(element.machine_id);
        console.log(index);
        console.log(arraydata[index]);
        arraydata[index] = arraydata[index] + element.meters;
      }
      //console.log(ids);
      
    }
    this.chartMetersMachine.labels = arraylabels;
    this.chartMetersMachine.data = arraydata;
    this.chartMetersMachine.backgrounds = arraybg;
    this.chartMetersMachine.title = 'Produccion';
    console.log(this.chartMetersMachine);
    let canvas = document.getElementById("chartMetersMachine");
    this.reportService.barChart(canvas, this.chartMetersMachine);
  }

  // Grafica de barras para urdimbre por operador
  getReportUrdimbreOperators(): void {
    console.log(this.data['data'] );
    let arraydata: any[] = [];
    let arraylabels: any[] = [];
    let arraybg: any[] = [];
    let ids: any[] = [];
    for (let i = 0; i < this.data.length; i++) {
      const element = this.data[i];
      //console.log(ids.includes(element.operator_id));
      if (!ids.includes(element.operator_id)) {
        ids[i] = element.operator_id;
        console.log(element.machine_id);
        arraydata[i] = element.urdimbre;
        arraylabels[i] = element.operator;
        arraybg[i] = this.reportService.getHexRandomColor();
      } else {
        let index = ids.indexOf(element.operator_id);
        console.log(index);
        console.log(arraydata[index]);
        arraydata[index] = arraydata[index] + element.urdimbre;
      }
      //console.log(ids);
      
    }
    this.chartUrdimbreOperator.labels = arraylabels;
    this.chartUrdimbreOperator.data = arraydata;
    this.chartUrdimbreOperator.backgrounds = arraybg;
    this.chartUrdimbreOperator.title = 'Produccion';
    console.log(this.chartUrdimbreOperator);
    let canvas = document.getElementById("chartUrdimbreOperator");
    this.reportService.barChart(canvas, this.chartUrdimbreOperator);
  }
  
  // Grafica de barras para urdimbre por maquina
  getReportUrdimbreMachines(): void {
    console.log(this.data['data'] );
    let arraydata: any[] = [];
    let arraylabels: any[] = [];
    let arraybg: any[] = [];
    let ids: any[] = [];
    for (let i = 0; i < this.data.length; i++) {
      const element = this.data[i];
      //console.log(ids.includes(element.machine_id));
      if (!ids.includes(element.machine_id)) {
        ids[i] = element.machine_id;
        console.log(element.machine_id);
        arraydata[i] = element.urdimbre;
        arraylabels[i] = element.machine_name;
        arraybg[i] = this.reportService.getHexRandomColor();
      } else {
        let index = ids.indexOf(element.machine_id);
        console.log(index);
        console.log(arraydata[index]);
        arraydata[index] = arraydata[index] + element.urdimbre;
      }
      //console.log(ids);
      
    }
    this.chartUrdimbreMachine.labels = arraylabels;
    this.chartUrdimbreMachine.data = arraydata;
    this.chartUrdimbreMachine.backgrounds = arraybg;
    this.chartUrdimbreMachine.title = 'Produccion';
    console.log(this.chartUrdimbreMachine);
    let canvas = document.getElementById("chartUrdimbreMachine");
    this.reportService.barChart(canvas, this.chartUrdimbreMachine);
  }

  exportExcel(){
    window.open(config.base_url + "/reports/exportReport", "_blank");
  }

  /* getReportMeters(): void {
    console.log(this.data['data'] );
    let arraydata: any[] = [];
    let arraylabels: any[] = [];
    let arraybg: any[] = [];
    for (let i = 0; i < this.data['data'].length; i++) {
      const element = this.data['data'][i];
      arraydata[i] = element.meters;
      arraylabels[i] = element.operator;
      arraybg[i] = this.reportService.getHexRandomColor();
    }
    this.report.labels = arraylabels;
    this.report.data = arraydata;
    this.report.backgrounds = arraybg;
    console.log(this.report);
    let canvas = document.getElementById("chartMeters");
    this.reportService.pieChart(canvas, this.report);
  }

  
  getReportUrdim(): void {
      console.log(this.data['data'] );
      let arraydata: any[] = [];
      let arraylabels: any[] = [];
      let arraybg: any[] = [];
      for (let i = 0; i < this.data['data'].length; i++) {
        const element = this.data['data'][i];
        arraydata[i] = element.urdim_percentage;
        arraylabels[i] = element.operator;
        arraybg[i] = this.reportService.getHexRandomColor();
      }
      this.reportUrdim.labels = arraylabels;
      this.reportUrdim.data = arraydata;
      this.reportUrdim.backgrounds = arraybg;
      console.log(this.reportUrdim);
      let canvas = document.getElementById("chartUrdim");
      this.reportService.pieChart(canvas, this.reportUrdim);
  }

  getReportEfi(): void {
    console.log(this.data['data'] );
    let arraydata: any[] = [];
    let arraylabels: any[] = [];
    let arraybg: any[] = [];
    for (let i = 0; i < this.data['data'].length; i++) {
      const element = this.data['data'][i];
      arraydata[i] = element.efi_percentage;
      arraylabels[i] = element.operator;
      arraybg[i] = this.reportService.getHexRandomColor();
    }
    this.reportEfi.labels = arraylabels;
    this.reportEfi.data = arraydata;
    this.reportEfi.backgrounds = arraybg;
    console.log(this.reportEfi);
    let canvas = document.getElementById("chartEfi");
    this.reportService.pieChart(canvas, this.reportEfi);
  }

  getHexColor(i: number, chart: any){
    return { 'color': chart.backgrounds[i] };
  } */
}
