import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GlobalApiService } from '../../../Core';
import { Location } from '@angular/common';
import { Employee } from '../../../model/employee';
import { Machine } from '../../../model/machine';

@Component({
  selector: 'app-control',
  templateUrl: './employee-control.component.html',
  styleUrls: ['./employee-control.component.css']
})
export class EmployeeControlComponent implements OnInit {

  employees: Employee[] = [];
  employeeMachines: Machine[] = [];

  constructor(
    private globalService: GlobalApiService,
    private route: ActivatedRoute,
    private location: Location
    ) {
      this.getTeamEmployees()
    
    }

  ngOnInit() {
  }

  getTeamEmployees(): void{
    this.globalService.routes.employee.getSuperVisorTeam()<any>().subscribe(
      response => {
        this.employees = response.data;
        console.log(this.employees);
      }
    );
  }

  getMachines(employee){
    this.globalService.routes.employee.getMachines()<any>(employee).subscribe(
      response => {
        this.employeeMachines = response.data;
        console.log(this.employeeMachines);
      }
    );
  }

}
