import { Component, OnInit } from '@angular/core';
import { GlobalApiService } from '../../../Core/global/global-service';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.scss']
})
export class EmployeeListComponent implements OnInit {

  data: any[] = [];
  columns: any[] = [
    { header: 'ID', field: 'id', visible: false },
    { header: 'Nombre', field: 'first_name', visible: true },
    { header: 'Apellido', field: 'last_name', visible: true },
    { header: 'Direccion', field: 'address', visible: true },
    { header: 'Telefono', field: 'phone', visible: true }
  ];
  Employees: any[];
  constructor(private globalService: GlobalApiService) { }

  ngOnInit() {
    this.getAll();
  }

  getAll(): void {
    this.globalService.routes.employee.getAll()<any[]>().subscribe(
      response => {

        this.data = response;
        console.log(this.data );
      },
      error => { }
    );
  }

}
