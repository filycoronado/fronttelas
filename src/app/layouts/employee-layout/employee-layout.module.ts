import { UserListComponent } from './users/user-list/user-list.component';
import { UserComponent } from './users/user/user.component';
import { DashboardModule } from './../../pages/dashboard/dashboard.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { UtilitiesModule } from '../../Core/utilities.module';
import { NgModule } from '@angular/core';
import { EmployeeLayoutRoutes } from './employee-layout.routing';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { EmployeeFormComponent } from '../employee-layout/Employee-Form/Employee-Form.component';
import { QuickAppProMaterialModule } from '../../modules/material.module';
import { EmployeeListComponent } from './employee-list/employee-list.component';
import { HorariosListComponent } from './horarios-list/horarios-list.component';
import { ScheduleeFormComponent } from './schedulee-form/schedulee-form.component';
import { EmployeeControlComponent } from './employee-control/employee-control.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(EmployeeLayoutRoutes),
    FormsModule,
    NgbModule,
    QuickAppProMaterialModule,
    ReactiveFormsModule,
    UtilitiesModule,
    FlexLayoutModule
  ],
  declarations: [
    EmployeeFormComponent,
    UserListComponent,
    UserComponent,
    EmployeeListComponent,
    HorariosListComponent,
    ScheduleeFormComponent,
    EmployeeControlComponent
  ],
  exports: [RouterModule]
})
export class EmployeeLayoutModule { }
