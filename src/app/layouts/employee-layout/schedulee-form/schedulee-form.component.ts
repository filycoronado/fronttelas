import { Component, OnInit } from '@angular/core';
import { Employee } from '../../../model/employee';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { Location } from '@angular/common';
import { Schedule } from '../../../model/schedule';
import { GlobalApiService } from '../../../Core/global/global-service';

@Component({
  selector: 'app-schedulee-form',
  templateUrl: './schedulee-form.component.html',
  styleUrls: ['./schedulee-form.component.scss']
})
export class ScheduleeFormComponent implements OnInit {


  // Variables
  isEdit = false
  showAdd = false;
  id: number;
  user: Schedule;


  // Forms
  userGroup: FormGroup;
  private turnos = [];
  constructor(private apiService: GlobalApiService,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private location: Location) { }

  ngOnInit() {
    this.initForm();
    this.route.params.subscribe(
      params => {
        this.id = params['id'];
        if (this.id && this.id > 0) {
          this.isEdit = true;
          this.getSchedule();
        }
      }
    );
  }
  getSchedule(): void {
    this.apiService.routes.schedules.findById()<Schedule>(this.id).subscribe(
      response => {
        this.user = response;
        this.patchValues();
      }
    );
  }

  setTunrn(): void {
    const tp = this.userGroup.get('schType').value;
    if (tp === '1') {
      this.turnos = [
        {

          name: 'T1'
        }, {

          name: 'T2'
        }
      ];
    } else if (tp === '2') {
      this.turnos = [
        {

          name: 'T1'
        }, {

          name: 'T2'
        }, {
          name: 'T3'
        }
      ];
    } else {
      this.turnos = [];
    }
  }
  patchValues(): void {
    this.userGroup.patchValue({
      schName: this.user.name,
      schType: this.user.type,
      blockType: this.user.block,
      TypeTurn: this.user.turn,

    });
  }
  initForm(): void {
    this.userGroup = this.formBuilder.group({
      schName: ['', [Validators.required]],
      schType: [-1, [Validators.required]],
      blockType: [-1, [Validators.required]],
      TypeTurn: [-1, [Validators.required]],

    });
  }
  parseValues(): void {
    this.user = {
      id: this.isEdit ? this.id : undefined,
      name: this.userGroup.get('schName').value,
      type: this.userGroup.get('schType').value,
      block: this.userGroup.get('blockType').value,
      turn: this.userGroup.get('TypeTurn').value
    };
  }
  saveOrUpdate(): void {
    if (this.userGroup.valid) {
      this.parseValues();
      this.isEdit ? this.update() : this.save();
    }
  }

  save(): void {
    this.apiService.routes.schedules.addSchedule()<any>(this.user).subscribe(
      response => { this.location.back(); },
      error => { }
    );
  }

  update(): void {
    this.apiService.routes.schedules.uppdateSchedule()<any>(this.user).subscribe(
      response => {
        this.location.back();
      },
      error => { }
    );
  }

  get title(): string {
    return this.isEdit ? 'Edición de horario' : 'Crea un horario';
  }

  back(): void {
    this.location.back();
  }

}
