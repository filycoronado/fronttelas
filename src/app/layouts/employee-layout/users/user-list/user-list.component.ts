import { User } from './../../../../model/user';
import { GlobalApiService } from './../../../../Core/global/global-service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
  // Variables
  title = 'Usuarios';
  userRoute = 'employee/users';
  showAdd = true;

  // Data variables
  users: User[];
  columns: any[] = [
    { header: 'ID', field: 'id', visible: false },
    { header: 'Nombre de Usuario', field: 'username', visible: true },
    { header: 'Empleado', field: 'first_name', visible: true },
    { header: 'Rol', field: 'role', visible: true }
  ];

  constructor(private apiService: GlobalApiService) {

  }

  ngOnInit() {
    this.apiService.routes.users.getAll()<any[]>().subscribe(
      response => {
         this.users = response;
         },
      error => console.log(error)
    );
  }

}
