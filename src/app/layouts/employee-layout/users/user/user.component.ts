import { NotificationsService } from './../../../../services/notifications.service';
import { Role } from './../../../../model/role';
import { Employee } from './../../../../model/employee';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { User } from './../../../../model/user';
import { GlobalApiService } from './../../../../Core/global/global-service';
import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Observable, forkJoin } from 'rxjs';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  // Variables
  isEdit = false
  showAdd = false;
  id: number;
  user: User;
  employees: Employee[];
  roles: Role[];
  loading = false;
  // Forms
  userGroup: FormGroup;

  constructor(private apiService: GlobalApiService,
              private route: ActivatedRoute,
              private formBuilder: FormBuilder,
              private location: Location,
              private notify: NotificationsService) { }

  ngOnInit() {
    this.initForm();
    this.getComboData();
  this.route.params.subscribe(
    params => {
      this.id = params['id'];
      if (this.id && this.id > 0) {
        this.isEdit = true;
        this.loading = true;
        this.getUser();
      }
    }
  );
  }
  getUser(): void {
    this.apiService.routes.users.getById()<User>(this.id).subscribe(
      response => {
        this.user = response;
        this.getEmployee();
        this.loading = false;
      }
    );
  }

  getRoles(): void {
    this.apiService.routes.roles.getAll()<Role[]>().subscribe(
      response => this.roles = response
    );
  }

  patchValues(): void {
    this.userGroup.patchValue({
      userName: this.user.username,
      password: this.user.password,
      employee: this.user.employee_id,
      role: this.user.role_id
    });
  }

  initForm(): void {
    this.userGroup = this.formBuilder.group({
      userName: ['', [Validators.required]],
      password: [''],
      employee: [-1],
      role: [-1]
    });
  }

  parseValues(): void {
    this.user = {
      id: this.isEdit ? this.id : undefined,
      username: this.userGroup.get('userName').value,
      password: this.userGroup.get('password').value,
      employee_id: this.userGroup.get('employee').value,
      role_id: Number(this.userGroup.get('role').value)
    };
  }

  saveOrUpdate(): void {
    if (this.userGroup.valid) {
      this.parseValues();
      this.isEdit ? this.update() : this.save();
    }
  }

  save(): void {
    this.loading = true;
    this.apiService.routes.users.addUser()<any>(this.user).subscribe(
      response => {
        this.notify.successMessage('Usuario guardado.')
         this.location.back();
         },
      error => {}
    );
  }

  update(): void {
    debugger;
    this.apiService.routes.users.updateUser()<any>(this.user).subscribe(
      response => {
        debugger;
         this.notify.infoMessage('Usuario modificado.')
         this.location.back();
         },
      error => {}
    );
  }

  get title(): string {
    return this.isEdit ? 'Edición de usuario' : 'Creación de Usuario';
  }

  getComboData() {
    this.apiService.routes.employee.availables()<Employee[]>().subscribe(response => {
       this.employees = response
    },
    error => console.log(error)
    );
    this.apiService.routes.roles.getAll()<Role[]>().subscribe(response => {
      this.roles = response
    });
  }

  back(): void {
    this.location.back();
  }

  getEmployee(): void {
    this.apiService.routes.employee.getById()<Employee>(this.user.employee_id).subscribe(
      response => {
        this.employees.push(response);
        this.patchValues();
      }
    );
  }
}
