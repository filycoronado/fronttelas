import { Component, OnInit } from '@angular/core';
import { Employee } from '../../../model/employee';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { GlobalApiService } from '../../../Core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Schedule } from '../../../model/schedule';
import { Employeeschedules } from '../../../model/employee_schedule';

@Component({
  selector: 'app-employee-form',
  templateUrl: './Employee-Form.component.html',
  styleUrls: ['./Employee-Form.component.scss']
})
export class EmployeeFormComponent implements OnInit {

  // Form variables
  testGroup: FormGroup;

  // Variables
  employee: Employee;
  employee_schedule: Employeeschedules;
  isEdit = false;
  id: number;
  schedules = [];

  constructor(private formBuilder: FormBuilder,
    private globalService: GlobalApiService,
    private route: ActivatedRoute,
    private location: Location) {

    this.initForm();
  }

  ngOnInit() {
    this.employee_schedule = new Employeeschedules();
    this.route.params.subscribe(
      params => {
        this.id = params['id'];
        if (this.id && this.id > 0) {
          this.isEdit = true;
          this.getEmployee();
          this.getSchedule();
          this.employee_schedule.employee_id = this.id;

        }
      }
    );
    this.getAll();
  }
  getSchedule(): void {
    this.globalService.routes.employee.getSchedule()<any>(this.id).subscribe(
      response => {
        this.employee_schedule.id = response.data[0].pivot.id;
        this.testGroup.patchValue({
          blockType: response.data[0].pivot.schedule_id,
        });

      }
    );
  }

  saveSchedule(): void {

    if (this.isEdit) {
      this.employee_schedule.schedule_id = this.testGroup.get('blockType').value;
      this.globalService.routes.schedules.saveEmployeeSchedule()<any>(this.employee_schedule).subscribe(
        response => {
          if (response.success === true) {
            alert(response.data);
          }
        },
        error => {

        }
      );
    }

  }

  saveEmployeeSchedule(): void {

  }

  getAll(): void {
    this.globalService.routes.schedules.getAll()<any[]>().subscribe(
      response => {
        this.schedules = response;
        response.forEach(x => {
          if (x.type === 1) {
            x.type = '12 Horas';
          } else if (x.type === 2) {
            x.type = '8 Horas';
          }

          if (x.block === '1') {
            x.block = 'Lunes-Jueves';
          } else if (x.block === '2') {
            x.block = 'Viernes-Domingo';
          }
        });
      },
      error => { }
    );
  }

  getEmployee(): void {
    this.globalService.routes.employee.getById()<Employee>(this.id).subscribe(
      response => {
        this.employee = response;
        this.patchValues(this.employee);
      }
    );
  }
  initForm(): void {
    this.testGroup = this.formBuilder.group({
      FirstName: ['', [Validators.required]],
      LastName: ['', [Validators.required]],
      address: ['', [Validators.required]],
      email: [''],
      phone: [''],
      blockType: ['-1'],
      isactive: [true]
    });
  }

  patchValues(p: Employee): void {
    this.testGroup.patchValue({
      FirstName: p.first_name,
      LastName: p.last_name,
      address: p.address,
      phone: p.phone,
      email: p.email,
      isactive: p.deleted_at === null ? true : false
    });
  }

  saveOrUpdate(): void {
    if (this.testGroup.valid) {
      const p = this.parseData();
      this.isEdit ? this.update(p) : this.save(p);
    }
  }

  save(p: Employee): void {
    this.globalService.routes.employee.addEmployee()<any>(p).subscribe(
      response => {

        this.employee_schedule.employee_id = response.data.id;
        this.employee_schedule.schedule_id = this.testGroup.get('blockType').value;
        this.globalService.routes.schedules.saveEmployeeSchedule()<any>(this.employee_schedule).subscribe(
          response2 => {
            if (response2.success === true) {
              alert(response2.data);
            }
          },
          error => {

          }
        );
        this.location.back();
      },
      error => { }
    );
  }

  update(p: Employee): void {
    this.globalService.routes.employee.uppdateEmployee()<any>(this.parseData()).subscribe(
      response => { this.location.back() },
      error => { }
    );
  }

  private parseData(): Employee {
    const p: Employee = {
      id: this.id,
      first_name: this.testGroup.get('FirstName').value,
      last_name: this.testGroup.get('LastName').value,
      address: this.testGroup.get('address').value,
      phone: this.testGroup.get('phone').value,
      email: this.testGroup.get('email').value
    }
    return p;
  }

  back(): void {
    this.location.back();
  }

  get title(): string {
    return this.isEdit ? 'Edición de empleado' : 'Creación de Empleado';
  }
}
