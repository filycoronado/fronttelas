import { Component, OnInit } from '@angular/core';
import { GlobalApiService } from '../../../Core';

@Component({
  selector: 'app-horarios-list',
  templateUrl: './horarios-list.component.html',
  styleUrls: ['./horarios-list.component.scss']
})
export class HorariosListComponent implements OnInit {

  data: any[] = [];
  columns: any[] = [
    { header: 'ID', field: 'id', visible: false },
    { header: 'Nombre', field: 'name', visible: true },
    { header: 'Bloque', field: 'block', visible: true },
    { header: 'Tipo', field: 'type', visible: true }
  ];
  Employees: any[];
  constructor(private globalService: GlobalApiService) { }

  ngOnInit() {
    this.getAll();
  }

  getAll(): void {
    this.globalService.routes.schedules.getAll()<any[]>().subscribe(
      response => {
        this.data = response;
        response.forEach(x => {
          if (x.type === 1) {
            x.type = '12 Horas';
          } else if (x.type === 2) {
            x.type = '8 Horas';
          }

          if (x.block === '1') {
            x.block = 'Lunes-Jueves';
          } else if (x.block === '2') {
            x.block = 'Viernes-Domingo';
          }
        });
      },
      error => { }
    );
  }
}
