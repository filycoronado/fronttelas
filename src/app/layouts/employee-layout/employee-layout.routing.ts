import { AuthGuard } from './../../services/authGuard.service';
import { UserListComponent } from './users/user-list/user-list.component';
import { UserComponent } from './users/user/user.component';
import { EmployeeListComponent } from './employee-list/employee-list.component';
import { EmployeeFormComponent } from './Employee-Form/Employee-Form.component';
import { Routes } from '@angular/router';
import { HorariosListComponent } from './horarios-list/horarios-list.component';
import { ScheduleeFormComponent } from './schedulee-form/schedulee-form.component';
import { EmployeeControlComponent } from './employee-control/employee-control.component';

export const EmployeeLayoutRoutes: Routes = [
{ path: 'employeeForm', component: EmployeeFormComponent, canActivate: [AuthGuard]  },
{ path: 'userList', component: UserListComponent, canActivate: [AuthGuard]  },
{ path: 'users/:id', component: UserComponent, canActivate: [AuthGuard]  },
{ path: 'users', component: UserComponent, canActivate: [AuthGuard]  },
{ path: 'list', component: EmployeeListComponent, canActivate: [AuthGuard] },
{ path: 'schedule/list', component: HorariosListComponent, canActivate: [AuthGuard]  },
{ path: 'schedule/form', component: ScheduleeFormComponent, canActivate: [AuthGuard]  },
{ path: 'schedule/form/:id', component: ScheduleeFormComponent, canActivate: [AuthGuard]  },
{ path: 'employeeForm/:id', component: EmployeeFormComponent, canActivate: [AuthGuard]  },
{ path: 'control', component: EmployeeControlComponent, canActivate: [AuthGuard]  },
];
