import { Component, OnInit } from '@angular/core';
import { GlobalApiService } from '../../../Core/global/global-service';

@Component({
  selector: 'app-machine-list',
  templateUrl: './machine-list.component.html',
  styleUrls: ['./machine-list.component.scss']
})
export class MachineListComponent implements OnInit {

  data: any[] = [];
  columns: any[] = [
    { header: 'ID', field: 'id', visible: false },
    { header: 'Pics', field: 'pics', visible: true },
    { header: 'Nombre', field: 'name', visible: true },
    { header: 'Trama', field: 'trama', visible: true }
  ];
  Machines: any[];

  constructor(private globalService: GlobalApiService) { }

  ngOnInit() {
    this.getAll();
  }

  getAll(): void {
    this.globalService.routes.machines.getAll()<any[]>().subscribe(
      response => {
        this.data = response;
        console.log(this.data );
      },
      error => { }
    );
  }

}
