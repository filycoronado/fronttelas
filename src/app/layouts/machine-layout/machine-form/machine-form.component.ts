import { Component, OnInit } from '@angular/core';
import { Machine } from '../../../model/machine';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GlobalApiService } from '../../../Core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-machine-form',
  templateUrl: './machine-form.component.html',
  styleUrls: ['./machine-form.component.scss']
})
export class MachineFormComponent implements OnInit {

  // Form variables
  testGroup: FormGroup;

  // Variables
  machine: Machine;
  isEdit = false;
  id: number;

  constructor(
      private formBuilder: FormBuilder,
      private globalService: GlobalApiService,
      private route: ActivatedRoute,
      private location: Location
    ) {
      this.initForm();
    }

  ngOnInit() {
    this.route.params.subscribe(
      params => {
        this.id = params['id'];
        if (this.id && this.id > 0) {
          this.isEdit = true;
          this.getMachine();
        }
      }
    );
  }

  getMachine(): void {
    this.globalService.routes.machines.getById()<Machine>(this.id).subscribe(
      response => {
        this.machine = response['data'];
        this.patchValues(this.machine);
      }
    );
  }

  initForm(): void {
    this.testGroup = this.formBuilder.group({
      Pics: ['', [Validators.required]],
      machineName: [''],
      trama: ['']
    });
  }

  patchValues(m: Machine): void {
    this.testGroup.patchValue({
      Pics: m.pics,
      machineName: m.name,
      trama: m.trama
    });
  }

  saveOrUpdate(): void {
    if (this.testGroup.valid) {
      const m = this.parseData();
      this.isEdit ? this.update(m) : this.save(m);
    }
  }

  save(m: Machine): void {
    this.globalService.routes.machines.addMachine()<Machine>(m).subscribe(
      response => {
        this.location.back();
      },
      error => { }
    );
  }

  update(m: Machine): void {
    this.globalService.routes.machines.updateMachine()<any>(this.parseData()).subscribe(
      response => { this.location.back() },
      error => {}
    );
  }

  private parseData(): Machine {
    const m: Machine = {
      id: this.id,
      pics: this.testGroup.get('Pics').value,
      name: this.testGroup.get('machineName').value,
      trama: this.testGroup.get('trama').value,
    }
    return m;
  }

  back(): void {
    this.location.back();
  }

  get title(): string {
    return this.isEdit ? 'Edición de maquina' : 'Creación de maquina';
  }

}
