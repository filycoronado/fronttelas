import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MachineLayoutRoutingModule } from './machine-layout-routing.module';
import { MachineListComponent } from './machine-list/machine-list.component';
import { MachineLayoutRoutes } from './machine-layout.routing';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { QuickAppProMaterialModule } from '../../modules/material.module';
import { UtilitiesModule } from '../../Core/utilities.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MachineFormComponent } from './machine-form/machine-form.component';


@NgModule({
  declarations: [MachineListComponent, MachineFormComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(MachineLayoutRoutes),
    FormsModule,
    NgbModule,
    QuickAppProMaterialModule,
    ReactiveFormsModule,
    UtilitiesModule,
    FlexLayoutModule
  ],
  exports: [RouterModule]
})
export class MachineLayoutModule { }
