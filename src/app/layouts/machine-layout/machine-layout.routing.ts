import { Routes } from '@angular/router';
import { MachineListComponent } from './machine-list/machine-list.component';
import { MachineFormComponent } from './machine-form/machine-form.component';

export const MachineLayoutRoutes: Routes = [
{ path: 'list', component: MachineListComponent },
{ path: 'machineForm', component: MachineFormComponent },
{ path: 'machineForm/:id', component: MachineFormComponent },
];
