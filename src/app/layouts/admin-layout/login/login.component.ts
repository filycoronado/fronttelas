import { NotificationsService } from './../../../services/notifications.service';
import { AuthenticationService } from './../../../services/authentication.service';
import { User } from './../../../model/user';
import { GlobalApiService } from './../../../Core/global/global-service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginGroup: FormGroup
  user: User;
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';
  show = false;

  constructor(private formBuilder: FormBuilder,
    private apiService: GlobalApiService,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService, private notify: NotificationsService) { }

  ngOnInit() {
    this.initForm();
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    // reset login status
    this.authenticationService.logout();
  }

  initForm(): void {
    this.loginGroup = this.formBuilder.group({
      userName: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });
  }

  doLogin(): void {
    this.parseData();
    this.onSubmit();
  }

  parseData(): void {
    this.user = {
      username: this.loginGroup.get('userName').value,
      password: this.loginGroup.get('password').value
    };
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginGroup.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginGroup.invalid) {
      return;
    }

    this.loading = true;
    this.authenticationService.login(this.user.username, this.user.password)
      .pipe(first())
      .subscribe(
        data => {
          this.notify.successMessage('Bienvenido ' + data.data.name);
          this.router.navigate([this.returnUrl]);
        },
        error => {
          this.error = error;
          this.loading = false;
        });
  }

}
