import { RolesListComponent } from './roles/roles-list/roles-list.component';
import { RolesComponent } from './roles/roles.component';
import { PermissionsComponent } from './permissions/permissions.component';
import { DashboardModule } from './../../pages/dashboard/dashboard.module';
import { SidebarModule } from './../../sidebar/sidebar.module';
import { GoalComponent } from './goals/goal/goal.component';
import { TeamComponent } from './teams/team/team.component';
import { TeamsListComponent } from './teams/teams-list/teams-list.component';
import { EmployeeLayoutModule } from './../employee-layout/employee-layout.module';
import { UtilitiesModule } from './../../Core/utilities.module';
import { QuickAppProMaterialModule } from './../../modules/material.module';
import { TestComponent } from './../../pages/test/test.component';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AdminLayoutRoutes } from './admin-layout.routing';

import { DashboardComponent } from '../../pages/dashboard/dashboard.component';
import { UserTemplateComponent } from '../../pages/user/user.component';
import { TableComponent } from '../../pages/table/table.component';
import { TypographyComponent } from '../../pages/typography/typography.component';
import { IconsComponent } from '../../pages/icons/icons.component';
import { MapsComponent } from '../../pages/maps/maps.component';
import { NotificationsComponent } from '../../pages/notifications/notifications.component';
import { UpgradeComponent } from '../../pages/upgrade/upgrade.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { EmployeeFormComponent } from '../employee-layout/Employee-Form/Employee-Form.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    NgbModule,
    QuickAppProMaterialModule,
    ReactiveFormsModule,
    UtilitiesModule,
    EmployeeLayoutModule,
    DashboardModule
  ],
  declarations: [
    UserTemplateComponent,
    TableComponent,
    UpgradeComponent,
    TypographyComponent,
    IconsComponent,
    MapsComponent,
    NotificationsComponent,
    TestComponent,
    TeamsListComponent,
    TeamComponent,
    GoalComponent,
    PermissionsComponent,
    RolesComponent,
    RolesListComponent
  ]
})

export class AdminLayoutModule {}
