import { NotificationsService } from './../../../services/notifications.service';
import { Role } from './../../../model/role';
import { ActivatedRoute } from '@angular/router';
import { GlobalApiService } from './../../../Core/global/global-service';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { PermissionsComponent } from './../permissions/permissions.component';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Location } from '@angular/common';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.scss']
})
export class RolesComponent implements OnInit {

  permissions: any = {};
  rolePermissions = '{}';
  rolesGroup: FormGroup;
  role: Role;
  id: number;
  isEdit = false;
  constructor(public dialog: MatDialog,
    private formBuilder: FormBuilder,
    private apiSvc: GlobalApiService,
    private route: ActivatedRoute,
    private notify: NotificationsService,
    private location: Location) { }

  ngOnInit() {
    this.getPermissions();
    this.initForm();
    this.route.params.subscribe(
      params => {
        this.id = params['id'];
        if (this.id > 0) {
          this.isEdit = true;
          this.getRole();
        }
      }
    );
  }

  patchData(): void {
    this.rolesGroup.patchValue(
      {
        name: this.role.name
      }
    );

  }

  parseData(): void {
    this.role = {
      id: this.id,
      name: this.rolesGroup.get('name').value,
      permissions: JSON.stringify(this.rolePermissions)
    };
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(PermissionsComponent, {
      width: '300px',
      data: { permissions: this.permissions }
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.permissions = result;
    });
  }

  initForm(): void {
    this.rolesGroup = this.formBuilder.group({
      name: ['', [Validators.required]]
    });
  }

  initToggle() {
    this.permissions.user.forEach((x: string) => this.rolesGroup.addControl('user' + x, new FormControl()));
    this.permissions.employee.forEach((x: string) => this.rolesGroup.addControl('employee' + x, new FormControl()));
    this.permissions.schedule.forEach((x: string) => this.rolesGroup.addControl('schedule' + x, new FormControl()));
    this.permissions.machine.forEach((x: string) => this.rolesGroup.addControl('machine' + x, new FormControl()));
    this.permissions.team.forEach((x: string) => this.rolesGroup.addControl('team' + x, new FormControl()));
  }

  saveOrUpdate(): void {
    this.parseData();
    this.isEdit ? this.update() : this.save();
  }

  save(): void {
    this.apiSvc.routes.roles.addRole()<any>(this.role).subscribe(
      () => {
         this.notify.successMessage('Rol creado.');
          this.back();
        }
    );
  }

  update(): void {
    this.apiSvc
    .routes.roles.updateRole()<any>(this.role).subscribe(
      () => {
        this.notify.successMessage('Rol modificado.');
        this.back();
      }
    );
  }
  getRole(): void {
    this.apiSvc.routes.roles.getById()<Role>(this.id).subscribe(
      response => {
         this.role = response;
         this.rolePermissions = this.role.permissions;
         this.patchData();
        }
    );
  }

  getPermissions(): void {
    this.apiSvc.routes.roles.getAllPermissions()<any>().subscribe(
      response => {
         this.permissions = response;
         this.initToggle();
        }
    );
  }

  back(): void {
    this.location.back();
  }
  addPermission(mod: string, permission: string) {
    debugger;
    let rp = JSON.parse(this.rolePermissions);
    if (rp[mod] !== undefined) {
      let aux: any [] = rp[mod];
      const i = aux.indexOf(permission);
      if (i >= 0) {
        aux.splice(i, 1);
      } else {
        aux.push(permission);
      }
      rp[mod] = aux;
    } else {
      rp[mod] = [];
      rp[mod].push(permission);
    }
    this.rolePermissions = JSON.stringify(rp)
  }
}
