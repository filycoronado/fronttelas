import { Role } from './../../../../model/role';
import { GlobalApiService } from './../../../../Core/global/global-service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-roles-list',
  templateUrl: './roles-list.component.html',
  styleUrls: ['./roles-list.component.scss']
})
export class RolesListComponent implements OnInit {
  data: Role[] = [];
  addRoute = 'roles';
  columns: any[] = [
    { header: 'Nombre', field: 'name', visible: true }
   ];
   loading = false;
  constructor(private apiSvc: GlobalApiService) { }

  ngOnInit() {
    this.loading = true;
    this.apiSvc.routes.roles.getAll()<Role[]>().subscribe(
      response => { this.data = response; this.loading = false; },
      error => this.loading = false
    );
  }

}
