import { GlobalApiService } from './../../../Core/global/global-service';
import { OnInit, Component } from '@angular/core';

@Component({
  selector: 'app-permissions',
  templateUrl: './permissions.component.html',
  styleUrls: ['./permissions.component.scss']
})
export class PermissionsComponent implements OnInit {

  permissions: any[] = [];
  constructor(private apiSvc: GlobalApiService) { }

  ngOnInit() {
  }

  getPermissions(): void {
    this.apiSvc.routes.roles.getAllPermissions()<any[]>().subscribe(
      response => this.permissions = response
    );
  }
}

