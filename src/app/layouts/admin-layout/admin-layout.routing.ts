import { RolesComponent } from './roles/roles.component';
import { RolesListComponent } from './roles/roles-list/roles-list.component';
import { AuthGuard } from './../../services/authGuard.service';
import { TeamComponent } from './teams/team/team.component';
import { TeamsListComponent } from './teams/teams-list/teams-list.component';
import { TestComponent } from '../../pages/test/test.component';
import { Routes } from '@angular/router';
import { DashboardComponent } from '../../pages/dashboard/dashboard.component';
import { UserTemplateComponent } from '../../pages/user/user.component';
import { TableComponent } from '../../pages/table/table.component';
import { TypographyComponent } from '../../pages/typography/typography.component';
import { IconsComponent } from '../../pages/icons/icons.component';
import { MapsComponent } from '../../pages/maps/maps.component';
import { NotificationsComponent } from '../../pages/notifications/notifications.component';
import { UpgradeComponent } from '../../pages/upgrade/upgrade.component';

export const AdminLayoutRoutes: Routes = [
    { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
    { path: 'user', component: UserTemplateComponent },
    { path: 'table', component: TableComponent },
    { path: 'typography', component: TypographyComponent },
    { path: 'icons', component: IconsComponent },
    { path: 'maps', component: MapsComponent },
    { path: 'notifications', component: NotificationsComponent },
    { path: 'upgrade', component: UpgradeComponent },
    { path: 'test', component: TestComponent },
    { path: 'test/:id', component: TestComponent },
    { path: 'teamsList', component: TeamsListComponent, canActivate: [AuthGuard] },
    { path: 'teams', component: TeamComponent, canActivate: [AuthGuard] },
    { path: 'teams/:id', component: TeamComponent, canActivate: [AuthGuard] },
    { path: 'rolesList', component: RolesListComponent, canActivate: [AuthGuard] },
    { path: 'roles', component: RolesComponent, canActivate: [AuthGuard] },
    { path: 'roles/:id', component: RolesComponent, canActivate: [AuthGuard] },
    {
      path: 'employee',
      children: [{
        path: '',
      loadChildren: () => import('./../employee-layout/employee-layout.module').then(m => m.EmployeeLayoutModule)
      }]},
      {
        path: 'testComponents',
        children: [{
          path: '',
          loadChildren: () => import('./../../modules/examples/test.module').then(m => m.TestModule)
        }]
      },    {
        path: 'machine',
        children: [{
          path: '',
            loadChildren: () => import('./../../layouts/machine-layout/machine-layout.module').then(m => m.MachineLayoutModule)
          }]
      },    {
        path: 'report',
        children: [{
          path: '',
            loadChildren: () => import('./../../layouts/report-layout/report-layout.module').then(m => m.ReportLayoutModule)
          }]
      }
];
