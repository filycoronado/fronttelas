import { GlobalApiService } from './../../../../Core/global/global-service';
import { Component, OnInit } from '@angular/core';
import { Team } from '../../../../model/team';

@Component({
  selector: 'app-teams-list',
  templateUrl: './teams-list.component.html',
  styleUrls: ['./teams-list.component.scss']
})
export class TeamsListComponent implements OnInit {

  // Variables
  title = 'Equipos regitrados';
  addRoute = 'teams'
  loading = false;
  data: Team[];
  columns: any[] = [
    { header: 'ID', field: 'id', visible: false },
    { header: 'Nombre', field: 'name', visible: true }
  ];
  constructor(private apiService: GlobalApiService) { }

  ngOnInit() {
    this.getTeams();
  }

  getTeams(): void {
    this.loading = true;
    this.apiService.routes.teams.getAll()<Team[]>().subscribe(
      response => {
         this.data = response;
         this.loading = false;
         },
         error => this.loading = false
    );
  }
}
