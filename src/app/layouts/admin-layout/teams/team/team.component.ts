import { NotificationsService } from './../../../../services/notifications.service';
import { TeamGoal } from './../../../../model/team-goal';

import { TeamEmployee } from './../../../../model/team-employee';
import { Goal } from './../../../../model/goal';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GlobalApiService } from './../../../../Core/global/global-service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Team } from '../../../../model/team';
import { Location } from '@angular/common';
import { GoalComponent } from '../../goals/goal/goal.component';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.scss']
})
export class TeamComponent implements OnInit {

  // ViewChilds
  @ViewChild(GoalComponent, {static: false}) goalView: GoalComponent;

  // Variables
  isEdit = false;
  loading = false;
  id = -1;
  goalId: number;
  team: Team;
  teamGroup: FormGroup;
  data: any[] = []
  goal: Goal;
  firstExtra = false;
  secondExtra = false;
  // Grid
  columns: any[] = [
    { header: 'Nombre', field: 'first_name', visible: true }
  ];
  employees: any[] = [];
  operators: any[] = [];
  supervisors: any[] = [];
  teamEmployees: any[] = [];
  schedules: any[] = [];
  machines: any[] = [];
  machines1: any [] = [];
  machines2: any [] = [];
  machines3: any [] = [];
  machines4: any [] = [];
  machines5: any [] = [];

  constructor(private apiService: GlobalApiService,
              private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private location: Location,
              private notify: NotificationsService) { }

  ngOnInit() {
    this.initTeamForm();
    this.getAvailablesEmployees();
    this.getMachines();
  //  this.getAvailableSupervisor();
    this.getSchedules();
    this.route.params.subscribe(params => {
      this.id = Number(params['id']);
      if (typeof this.id !== 'undefined' && this.id > 0) {
        this.isEdit = true;
        this.loading = true;
        this.getTeam();
      }
    });
  }

  saveOrUpdate(): void {
    if (this.teamGroup.valid) {
      this.parseValues();
      this.isEdit ? this.update() : this.save();
    }
  }

  getSchedules(): void {
    this.apiService.routes.schedules.getAll()<any[]>().subscribe(
      response => this.schedules = response
    );
  }

  getEmployeeMachines(): void {
    this.teamEmployees.forEach(x => {
      this.apiService.routes.employeeMachines.getByEmployee(x.id)<any[]>().subscribe(
        response => {
          if (x.machines === undefined) {
            x.machines = [];
          }
          response.forEach(z => {
             x.machines.push(z.id);
            });
        }
      );
    });
  }

  save(): void {
    this.loading = true;
      this.apiService.routes.teams.addTeam()<any>(this.team).subscribe(
        response => {
          this.id = response.id;
          this.data.forEach(x => x.team_id = this.id);
          const employeeMachines: any[] = [];
          this.teamEmployees.forEach(x => {
            x.machines.forEach((z, i) => {
              employeeMachines.push({employee_id: x.id, machine_id: z, extra_machine: (i > 2), workshift: true, active: true});
            });
          });
          this.apiService.routes.employeeMachines.add()<any>(employeeMachines).subscribe(
            () => {
              this.notify.successMessage('Maquinas agregadas.');
              this.loading = false;
            },
            error => this.loading = false
          );
          this.apiService.routes.teamsEmployees.syncTeamsEmployees()<any>(this.data).subscribe(
            () => { this.notify.successMessage('Empleados agregados al equipo.');
             },
            error => console.log('error: ' + error)
          );
        }
      );
      this.back();
  }

  update(): void {
    this.loading = true;
    debugger;
    this.apiService.routes.teams.updateTeam()<any>(this.team).subscribe(
      response => {
         this.notify.infoMessage('Equipo actualizado.');
         this.loading = false;
         this.back();
        },
      error => this.loading = false
    );
  }

  parseValues(): void {
    this.team = {
      id: this.id,
      name: this.teamGroup.get('name').value,
      supervisor_id: this.teamGroup.get('operatorsSelect').value,
      schedule_id: this.teamGroup.get('scheduleSelect').value
    }
  }

  getTitle(): string {
    return this.isEdit ? 'Edición de equipo de trabajo' : 'Registro de equipo de trabajo';
  }

  initTeamForm(): void {
    this.teamGroup = this.formBuilder.group({
      name: ['', [Validators.required]],
      operatorsSelect: [''],
      scheduleSelect: ['']
    });
  }

  patchTeamForm(): void {
    this.teamGroup.patchValue({
      name: this.team.name
    });
  }

  getTeam(): void {
    this.apiService.routes.teams.getById()<Team>(this.id).subscribe(
      response => {
      this.team = response;
      this.patchTeamForm();
      this.getTeamEmployees();
      this.loading = false;
      },
      error => console.log(error)
    );
  }

  getAvailablesEmployees(): void {
    this.apiService.routes.employee.getAll()<any[]>().subscribe(
      response => {
         this.employees = response;
      },
      error => console.log(error)
    );
  }

  getTeamEmployees(): void {
    this.apiService.routes.teamsEmployees.getTeamEmployees(this.id)<any[]>().subscribe(
      te => {
        let aux: any [] = [];
        te.forEach(x => {
          aux.push(x.employee);
       });
       this.teamEmployees = aux;
       aux = [];
       te.forEach(x => aux.push({id: x.id, team_id: x.team_id, employee_id: x.employee.id, active: x.active}));
       this.data = aux;
       this.getEmployeeMachines();
      }
    );
  }

  add(e: any) {
    let aux: any[] = [];
    this.teamEmployees.forEach(x => aux.push(x));
    aux.push(e);
    aux.forEach(x => {
      if (x.machines === undefined) {
      x.machines = [];
      x.machines.push({});
      x.machines.push({});
      x.machines.push({});
      }
    });
    this.teamEmployees = aux;
    this.syncTeamEmployee(e, true, false);
    aux = [];
    const i = this.employees.indexOf(e);
    this.employees.splice(i, 1);
    this.employees.forEach(x => aux.push(x));
    this.employees = aux;
  }

  delete(e: any): void {
    let aux: any[] = [];
    this.employees.forEach(x => aux.push(x));
    if (e.machines !== undefined && e.machines.length > 0) {
      e.marchihes.forEach(x => x.active = false);
    }
    aux.push(e);
    aux.sort((a, b) => a.id - b.id);
    this.employees = aux;
    this.syncTeamEmployee(e, false, true);
    aux = [];
    const i = this.teamEmployees.indexOf(e);
    this.teamEmployees.splice(i, 1);
    this.teamEmployees.forEach(x => aux.push(x));
    this.teamEmployees = aux;
  }

  back(): void {
    this.location.back();
  }

  getAvailableSupervisor() {
    this.apiService.routes.employee.getAvailableSupervisor()<any[]>().subscribe(
      response => this.supervisors = response
    );
  }

  addExtraMachine(id: any) {

  }

  getMachines(): void {
    this.apiService.routes.machines.getAll()<any[]>().subscribe(
      response => {
        this.machines = response;
        this.machines1 = this.machines;
        this.machines2 = this.machines;
        this.machines3 = this.machines;
        this.machines4 = this.machines;
        this.machines5 = this.machines;
      }
    );
  }

  syncTeamEmployee(e: any, active: boolean, remove: boolean) {
    let te;
    this.data.forEach(x => { if (x.employee_id === e.id) { te = x; } })
    if (te !== undefined) {
      const i = this.data.indexOf(te);
      if (te.id !== undefined) {
        te.active = active;
        this.data[i] = te;
      } else {
        if (remove) {
          this.data.splice(i, 1);
        }
      }
    } else {
      this.data.push({id: undefined, team_id: undefined, employee_id: e.id, active: active});
    }
  }

  showExtra(e: any): void {
    if (e.machines.length < 5) {
      e.machines.push({});
    }
  }
}
