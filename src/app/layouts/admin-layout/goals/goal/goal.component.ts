import { GlobalApiService } from './../../../../Core/global/global-service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';
import * as M from 'moment';
import { Goal } from '../../../../model/goal';
import { Location } from '@angular/common';

@Component({
  selector: 'app-goal',
  templateUrl: './goal.component.html',
  styleUrls: ['./goal.component.scss']
})
export class GoalComponent implements OnInit {

  // Inputs
  @Input() title: string;

  // Forms
  goalGroup: FormGroup

  // Variables
  goal: Goal;

  constructor(private formBuilder: FormBuilder,
              private apiService: GlobalApiService,
              private location: Location) { }

  ngOnInit() {
    this.initForm();
  }

  initForm(): void {
    this.goalGroup = this.formBuilder.group({
    //  name: ['', [Validators.required]],
      description: [''],
      date: [M()]
    });
  }

  parseValues(): void {
    this.goal = {
     // name: this.goalGroup.get('name').value,
      description: this.goalGroup.get('description').value,
      period: 'periodo 1',
      date: new Date()
    };
  }

  save() {
  this.parseValues();
  this.apiService.routes.goals.addGoal()<any>(this.goal).subscribe(
    response => {
      this.location.back();
      }
    );
  }

  public saveFromTeam(): Goal {
    this.parseValues();
    return this.goal;
  }

  public set setGoal(goal: Goal) {
    this.goal = goal;
    this.parseValues();
  }
}
