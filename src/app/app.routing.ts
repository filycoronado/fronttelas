import { AuthGuard } from './services/authGuard.service';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { Routes } from '@angular/router';

import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { LoginComponent } from './layouts/admin-layout/login/login.component';

export const AppRoutes: Routes = [

  { path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full' },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: '',
    component: AdminLayoutComponent,
    children: [
        {
      path: '',
      loadChildren: () => import('./layouts/admin-layout/admin-layout.module').then(m => m.AdminLayoutModule)
  }]},
  {
    path: '**',
    redirectTo: ''
  }
]
