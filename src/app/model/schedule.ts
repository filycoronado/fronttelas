export interface Schedule {
    id?: number;
    name?: string;
    type?: string;
    block?: string;
    turn?: string;
    created_at?: Date;
    updated_at?: Date;
    deleted_at?: Date;
}
