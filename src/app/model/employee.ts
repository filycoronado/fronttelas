export interface Employee {
  id?: number;
  first_name?: string;
  last_name?: string;
  email?: string;
  address?: string;
  phone?: string;
  deleted_at?: Date;
}
