export interface Person {
  name: string;
  lastName?: string;
  email?: string;
  phone?: string;
  address?: string;
  controlNumber?: string;
}
