export interface TeamEmployee {
  id?: number;
  employee_id?: number;
  team_id?: number;
  active?: boolean;
  created_at?: Date;
  updated_at?: Date;
  deleted_at?: Date;
}
