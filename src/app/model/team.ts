export interface Team {
  id?: number;
  name?: string;
  supervisor_id?: number;
  schedule_id?: number;
  created_at?: Date;
  updated_at?: Date;
  deleted_at?: Date;
}
