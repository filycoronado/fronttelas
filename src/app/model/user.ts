export interface User {
  id?: number;
  employee_id?: number;
  username?: string;
  password?: string;
  role_id?: number;
  remember_token?: string;
  created_at?: Date;
  updated_at?: Date;
  deleted_at?: Date;
  first_name?: string;
  role?: string;
}
