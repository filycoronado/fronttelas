export interface Goal {
  id?: number;
  name?: string;
  date?: Date;
  period?: string;
  description: string;
  created_at?: Date;
  updated_at?: Date;
  deleted_at?: Date;
}
