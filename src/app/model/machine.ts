export interface Machine {
    id?: number;
    pics?: number;
    name?: string;
    trama?: number;
    created_at?: Date;
    updated_at?: Date;
    deleted_at?: Date;
  }
