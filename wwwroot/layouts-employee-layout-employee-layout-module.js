(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["layouts-employee-layout-employee-layout-module"],{

/***/ "./src/app/layouts/employee-layout/employee-layout.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/layouts/employee-layout/employee-layout.module.ts ***!
  \*******************************************************************/
/*! exports provided: EmployeeLayoutModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeeLayoutModule", function() { return EmployeeLayoutModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _Core_utilities_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../Core/utilities.module */ "./src/app/Core/utilities.module.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _employee_layout_routing__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./employee-layout.routing */ "./src/app/layouts/employee-layout/employee-layout.routing.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm2015/ng-bootstrap.js");
/* harmony import */ var _employee_layout_Employee_Form_Employee_Form_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../employee-layout/Employee-Form/Employee-Form.component */ "./src/app/layouts/employee-layout/Employee-Form/Employee-Form.component.ts");
/* harmony import */ var _modules_material_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../modules/material.module */ "./src/app/modules/material.module.ts");










let EmployeeLayoutModule = class EmployeeLayoutModule {
};
EmployeeLayoutModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterModule"].forChild(_employee_layout_routing__WEBPACK_IMPORTED_MODULE_3__["EmployeeLayoutRoutes"]),
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"],
            _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__["NgbModule"],
            _modules_material_module__WEBPACK_IMPORTED_MODULE_9__["QuickAppProMaterialModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__["ReactiveFormsModule"],
            _Core_utilities_module__WEBPACK_IMPORTED_MODULE_1__["UtilitiesModule"]
        ],
        declarations: [_employee_layout_Employee_Form_Employee_Form_component__WEBPACK_IMPORTED_MODULE_8__["EmployeeFormComponent"]]
    })
], EmployeeLayoutModule);



/***/ }),

/***/ "./src/app/layouts/employee-layout/employee-layout.routing.ts":
/*!********************************************************************!*\
  !*** ./src/app/layouts/employee-layout/employee-layout.routing.ts ***!
  \********************************************************************/
/*! exports provided: EmployeeLayoutRoutes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeeLayoutRoutes", function() { return EmployeeLayoutRoutes; });
/* harmony import */ var _services_authGuard_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../../services/authGuard.service */ "./src/app/services/authGuard.service.ts");

const EmployeeLayoutRoutes = [
    { path: '', component: null, canActivate: [_services_authGuard_service__WEBPACK_IMPORTED_MODULE_0__["AuthGuard"]] },
    {}
];


/***/ })

}]);
//# sourceMappingURL=layouts-employee-layout-employee-layout-module.js.map